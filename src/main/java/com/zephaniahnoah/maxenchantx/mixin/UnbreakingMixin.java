package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.DigDurabilityEnchantment;

@Mixin(DigDurabilityEnchantment.class)
public class UnbreakingMixin {
	
	private static final int value = Main.unbreaking;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
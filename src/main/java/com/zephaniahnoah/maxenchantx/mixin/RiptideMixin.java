package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.TridentRiptideEnchantment;

@Mixin(TridentRiptideEnchantment.class)
public class RiptideMixin {
	
	private static final int value = Main.riptide;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
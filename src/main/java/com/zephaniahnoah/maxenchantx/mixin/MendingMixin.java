package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import net.minecraft.world.item.enchantment.MendingEnchantment;

@Mixin(MendingEnchantment.class)
public class MendingMixin {

	@Overwrite
	public boolean isTreasureOnly() {
		return false;
	}
}
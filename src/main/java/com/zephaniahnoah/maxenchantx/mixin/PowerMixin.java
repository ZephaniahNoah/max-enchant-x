package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.ArrowDamageEnchantment;

@Mixin(ArrowDamageEnchantment.class)
public class PowerMixin {
	
	private static final int value = Main.power;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
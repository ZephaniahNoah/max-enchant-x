package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.FishingSpeedEnchantment;

@Mixin(FishingSpeedEnchantment.class)
public class LureMixin {
	
	private static final int value = Main.lure;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
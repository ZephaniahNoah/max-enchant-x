package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.OxygenEnchantment;

@Mixin(OxygenEnchantment.class)
public class RespirationMixin {
	
	private static final int value = Main.respiration;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
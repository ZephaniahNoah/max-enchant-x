package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.FrostWalkerEnchantment;

@Mixin(FrostWalkerEnchantment.class)
public class FrostWalkerMixin {
	
	private static final int value = Main.frostWalker;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
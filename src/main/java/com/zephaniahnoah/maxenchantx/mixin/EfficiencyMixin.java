package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.DiggingEnchantment;

@Mixin(DiggingEnchantment.class)
public class EfficiencyMixin {

	private static final int value = Main.efficiency;
	
	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
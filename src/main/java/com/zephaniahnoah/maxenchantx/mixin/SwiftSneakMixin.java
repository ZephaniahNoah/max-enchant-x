package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.SwiftSneakEnchantment;


@Mixin(SwiftSneakEnchantment.class)
public class SwiftSneakMixin {
	
	private static final int value = Main.swiftSneak;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
	
	@Overwrite
	public boolean isTreasureOnly() {
		return false;
	}
}
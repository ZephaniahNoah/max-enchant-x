package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.DamageEnchantment;

@Mixin(DamageEnchantment.class)
public class SharpnessMixin {
	
	private static final int value = Main.sharpness;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
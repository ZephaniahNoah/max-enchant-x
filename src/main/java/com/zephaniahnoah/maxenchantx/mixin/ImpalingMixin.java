package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.TridentImpalerEnchantment;

@Mixin(TridentImpalerEnchantment.class)
public class ImpalingMixin {
	
	private static final int value = Main.impaling;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
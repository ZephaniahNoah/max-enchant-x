package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.SweepingEdgeEnchantment;

@Mixin(SweepingEdgeEnchantment.class)
public class SweepingEdgeMixin {
	
	private static final int value = Main.sweepingEdge;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
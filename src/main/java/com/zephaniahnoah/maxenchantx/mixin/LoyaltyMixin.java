package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.TridentLoyaltyEnchantment;

@Mixin(TridentLoyaltyEnchantment.class)
public class LoyaltyMixin {
	
	private static final int value = Main.loyalty;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
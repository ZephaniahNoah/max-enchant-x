package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.ArrowKnockbackEnchantment;

@Mixin(ArrowKnockbackEnchantment.class)
public class ArrowKnockbackMixin {
	
	private static final int value = Main.arrowKnockback;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.ArrowPiercingEnchantment;

@Mixin(ArrowPiercingEnchantment.class)
public class PiercingMixin {
	
	private static final int value = Main.piercing;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
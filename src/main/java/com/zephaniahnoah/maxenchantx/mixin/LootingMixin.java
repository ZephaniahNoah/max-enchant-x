package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.LootBonusEnchantment;

@Mixin(LootBonusEnchantment.class)
public class LootingMixin {
	
	private static final int value = Main.looting;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
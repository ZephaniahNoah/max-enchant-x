package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.KnockbackEnchantment;

@Mixin(KnockbackEnchantment.class)
public class KnockbackMixin {
	
	private static final int value = Main.knockback;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
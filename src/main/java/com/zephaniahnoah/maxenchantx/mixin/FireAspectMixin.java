package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.FireAspectEnchantment;

@Mixin(FireAspectEnchantment.class)
public class FireAspectMixin {
	
	private static final int value = Main.fireAspect;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
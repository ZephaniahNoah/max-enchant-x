package com.zephaniahnoah.maxenchantx.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import com.zephaniahnoah.maxenchantx.Main;

import net.minecraft.world.item.enchantment.ThornsEnchantment;

@Mixin(ThornsEnchantment.class)
public class ThornsMixin {
	
	private static final int value = Main.thorns;

	@Overwrite
	public int getMaxLevel() {
		return value;
	}
}
package com.zephaniahnoah.maxenchantx;

import net.minecraftforge.fml.common.Mod;

@Mod(Main.MODID)
public class Main {

	public static Config config;

	static {
		config = new Config("config");
		config.load();
	}

	public static final String MODID = "maxenchantx";
	public static final int arrowKnockback = config.getInt("punch");
	public static final int efficiency = config.getInt("efficiency");
	public static final int fireAspect = config.getInt("fire_aspect");
	public static final int frostWalker = config.getInt("frost_walker");
	public static final int impaling = config.getInt("impaling");
	public static final int knockback = config.getInt("knockback");
	public static final int looting = config.getInt("fortune/looting/luck");
	public static final int loyalty = config.getInt("loyalty");
	public static final int lure = config.getInt("lure");
	public static final int piercing = config.getInt("piercing");
	public static final int power = config.getInt("power");
	public static final int protection = config.getInt("protection");
	public static final int respiration = config.getInt("respiration");
	public static final int riptide = config.getInt("riptide");
	public static final int sharpness = config.getInt("sharpness");
	public static final int soulSpeed = config.getInt("soul_speed");
	public static final int sweepingEdge = config.getInt("sweeping_edge");
	public static final int thorns = config.getInt("thorns");
	public static final int unbreaking = config.getInt("unbreaking");
	public static final int swiftSneak = config.getInt("swiftSneak");

	static {
		config.saveConfig();
	}
}